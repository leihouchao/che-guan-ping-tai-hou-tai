let Redis = require('ioredis');

let redisCfg = require('./../config.json').redis;

//redis client
let xcRedis = new Redis({ 
    keyPrefix:`xc_ebike_`,//前缀
    host:redisCfg.host,
    port:redisCfg.port,
    passport:redisCfg.pwd,
    family:4,
});



module.exports = {
    xcRedis,//设备操作
};

