
const LoggerUtil = require('xclogutil').LoggerUtil;
const logger = new LoggerUtil('eBikeServer', '/admin');
logger.setLevel('INFO');

module.exports = {
    logger,
}