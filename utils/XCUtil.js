
/**
 * 检查req的body里是否包含paramKey  用于POST请求
 * @param  {[type]} req      [description]
 * @param  {[type]} paramKey [description]
 * @return {[type]}          [description]
 */
const existParam = (req,paramKey)=>{

	return req.body.hasOwnProperty(paramKey);
    // return req.params.hasOwnProperty(paramKey);

};
/**
 * 检查req的query里是否包含queryKey  用于GET请求
 * @param  {[type]} req      [description]
 * @param  {[type]} queryKey [description]
 * @return {[type]}          [description]
 */
const existQuery = (req,queryKey)=>{
    return req.query.hasOwnProperty(queryKey);
};
/**
 * 检查req的params里是否包含IMEI  用于GET请求
 * @param  {[type]} req      [description]
 * @param  {[type]} params   [description]
 * @return {[type]}          [description]
 */
const existIMEI = (req,queryKey)=>{
    return req.params.hasOwnProperty(queryKey);
};
/**
 * 检查某字典中是否包含paramKey
 * @param  {[type]} param    [description]
 * @param  {[type]} paramKey [description]
 * @return {[type]}          [description]
 */
const checkParamKey = (param,paramKey)=>{
    return param.hasOwnProperty(paramKey);
}
/**
 * 检查输入的字符串是否为指定的位数,以及返回是否需要补零
 * @param {[type]} param  input query
 * @param {[type]} bits   input bits
 * @param {[type]} isAdd  add zero
 * @return [param, true/false]
 */
const checkSetBits = (param, bits, IsAdd) => {
    const length = param.length;
    let isAdd = IsAdd || true;
    if (length !== bits) {
        if (isAdd === true) {
            (function () {
                let preAdd = '';
                for(let i=0;i< (bits - length);i++) {
                    preAdd += '0';
                }
                param = preAdd + param;
            }());
            return [param, false];
        } else {
            return [param, false];
        }
    } else {
        return [param,true];
    }
}
/**
 * 检查某IMEI长度合法性
 * @param  {[type]} IMEI    [description]
 * @param  {[type]}         [description]
 * @return {[type]}         [description]
 */
const checkIMEI = (IMEI)=>{
    return IMEI.length === 15;
}

// const xcShell = (cb)={

// 	try{
// 		cb();

// 	}catch(err){


// 	}finally{
// 		return next();
// 	}

// };


module.exports = {
	existParam,
    existQuery,
    existIMEI,
	// xcShell,
    checkParamKey,
    checkIMEI,
    checkSetBits
}