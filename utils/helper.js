/**
 * 辅助函数
 */

const moment = require('moment');

function createTradeNumber() {
    return moment().format('YYYYMMDD') + randomString(24, '1234567890');
}

function randomString(length, chars) {
    let result = '';
    for (let i = length; i > 0; --i) {
        result += chars[Math.floor(Math.random() * chars.length)];
    }
    return result;
}


module.exports = {
    createTradeNumber,
}