/**
 * Created by dingyuan on 2017.11.13
 */
'use strict';

const config = require('./../config.json');
const redis = require('redis');
const logger = require('./XCLogUtil').logger;

const RDS_OPTS = {auth_pass: config.redis_cli.pwd};
const client = redis.createClient(config.redis_cli.port, config.redis_cli.host, RDS_OPTS);

client.on('error', function (err) {
    logger.error('XCRedisUtils.js transparent transmission: redis client error: '+ err);
  });
client.on('connect', () => {
    logger.info('XCRedisUtils.js redisClient get into the connect');
});
client.on('reconnecting', () => {
    logger.info('XCRedisUtils.js redisClient reconnecting');
});


module.exports = {
    client,
}