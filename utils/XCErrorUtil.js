
const XC_BIZ_ERROR_TYPE = {
    BIZ_ERROR:'BIZ_ERROR',
    INPUT_PARAMS_MISS:'INPUT_PARAMS_MISS',
    NOT_EXIST_DEVICE:'NOT_EXIST_DEVICE'
}


class XCError{

    constructor(nativeErr, xcErrType, promot){

        this.errMsg = xcErrType;
        this.promot = promot;
        this.err = nativeErr;
        
        switch(xcErrType){
            case XC_BIZ_ERROR_TYPE.INPUT_PARAMS_MISS:{
                this.bizErrMsg = '入参缺失';
                this.errCode = 1;
                break;
            }
            case XC_BIZ_ERROR_TYPE.NOT_EXIST_DEVICE: {
                this.bizErrMsg = '该代理商无设备';
                this.errCode = 2;
                break;
            }
            default:{
                this.bizErrMsg = '业务错误';
                this.errCode = 0;
            }
        }

    }
	
}


module.exports = {
	XC_BIZ_ERROR_TYPE,
	XCError,
}