/**
 * 链接数据库
 */

const logger = require('./XCLogUtil').logger;
const Sequelize = require('sequelize');
const dbCfg = require('./../config').mysql;
// const dbCfg = require('./../config').mysqlLocal;
// const dbCfg = require('./../config').mysqlTest;

/**
 * db访问创建函数
 * @param {*} database 
 */
let SequelizeCreate = function (database) {
    return new Sequelize(database || dbCfg.database, dbCfg.user, dbCfg.password, {
        host: dbCfg.host,
        port: dbCfg.port,
        dialect: 'mysql',
        logging: (sql) => {
            logger.debug(sql);
        },
        benchmark: true,
        pool: {
            max: 100,
            min: 0,
            idle: 30000
        }
    });
}

/**
 * 创建全局db，默认是gps库
 */
const gSequelize = SequelizeCreate('gps');
gSequelize.authenticate().then(() => {
    logger.info('---------db connect suc');
    console.log('---------db connect suc');
}).catch((err) => {
    logger.error('---------db connect fail', err);
    console.log('---------db connect fail', err);
})

/**
 * 创建scm db
 */
const scmSequelize = SequelizeCreate('scm');
scmSequelize.authenticate().then(() => {
    logger.info('---------db connect suc');
    console.log('---------db connect suc');
}).catch((err) => {
    logger.error('---------db connect fail', err);
    console.log('---------db connect fail', err);
})



module.exports = {
    gSequelize,
    scmSequelize,
};
