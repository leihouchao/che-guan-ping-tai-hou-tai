
const XCEBike = require('xcdsutils');
const mysqlDB = require('./XCDBUtil').gSequelize;
const scmDB = require('./XCDBUtil').scmSequelize;
const redis = require('./XCEBikeRedisUtil').xcRedis;

XCEBike.config(redis, scmDB);

module.exports = {
    XCEBike,
}
