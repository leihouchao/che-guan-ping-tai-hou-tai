/*
 *created by dingyuan on 20171107
 */

const fs = require('fs');
const cors = require('cors');
const restify = require('restify');
const plugins = require('restify-plugins');
const routers = require('./routes/routes');


const http_options = {
    name: 'XCeBike',
    version: '1.0.0' 
}
const http_server = restify.createServer(http_options);

// https_options = {

// }
// const https_server = restify.createServer(https_options);

const acceptable = [
    'application/json',
    'text/plain',
    'application/octet-stream',
    'application/javascript',
    'audio/mp3',
    'audio/AMR'
];

let setup_server = (app) => {
    app.use(cors());
    app.use(plugins.bodyParser());
    app.use(plugins.queryParser());
    app.use(plugins.authorizationParser());
    app.use(plugins.acceptParser(acceptable));

    routers.applyRoutes(app); 
}

setup_server(http_server);

http_server.listen(8083, '0.0.0.0', () => {
    console.log('%s listening at %s', http_server.name, http_server.url);
})
