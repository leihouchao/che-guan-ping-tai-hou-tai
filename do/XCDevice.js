let XCError = require('./../utils/XCErrorUtil').XCError;
let XC_BIZ_ERROR_TYPE = require('./../utils/XCErrorUtil').XC_BIZ_ERROR_TYPE;

const Sequelize = require('sequelize');
let gSequelize = require('./../utils/XCDBUtil').scmSequelize;

let Utils = require('./../utils/XCUtil');
let checkParamKey = Utils.checkParamKey;

const XCDeviceModel = {
    LINE_2: 2,//两线
    LINE_4: 4,//4线
}


const DEVICE_TABLE_HEAD = 'xc_scm_';
const DEVICE_TABLE_TAIL = '_devices';

/**
 * 同一个设备出库入口是在一条记录里操作的
 */
const XCDeviceDAOStruct = {
    snCode: {
        type: Sequelize.STRING(16),
        primaryKey: true,
        allowNull: false,
    },
    imei: {
        type: Sequelize.STRING(16),
        allowNull: false,
        unique: true
    },
    //设备型号
    deviceModel: {
        type: Sequelize.INTEGER.UNSIGNED,
    },
    isInput: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
    },
    isOutput: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
    },
    //对应的入库记录ID，对于入库必填
    inputRecordId: {
        type: Sequelize.INTEGER.UNSIGNED,
    },
    //出库字段，对于出库必填
    fromAgentId: {
        type: Sequelize.INTEGER.UNSIGNED,
    },
    //出库字段，对于出库必填
    outputRecordId: {
        type: Sequelize.INTEGER.UNSIGNED,
    },
    //出库字段，出库记录被确认过后改写此字段为true
    outputConfirmed: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
    },
    //离线数据的字段，判断该设备是否激活过
    isActived: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
    },
    //是否被退货的标志字段
    isRefunded: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
        allowNull: false
    }
}


function XCDevice() {

}

XCDevice.daoWithAgentId = async function (agentId) {
    if (agentId) {
        let agentDeviceTableName = DEVICE_TABLE_HEAD + agentId + DEVICE_TABLE_TAIL;
        return gSequelize.define(agentDeviceTableName, XCDeviceDAOStruct);
    }
    throw new XCError(null, null, 'XCDevice.daoWithAgentId Failed agentId null');
}

XCDevice.getAgentAllImei = async function (agentId) {
    let deviceDAO = await XCDevice.daoWithAgentId(agentId);
    let imeiDAO = await deviceDAO.findAll({
        "attributes": ['imei']
    });
    if (!imeiDAO) {
        throw XC_BIZ_ERROR_TYPE.NOT_EXIST_DEVICE;
    }
    return imeiDAO;
}

XCDevice.getImeiByPageSize = async function (agentId, countPerpage, currPage) {
    let deviceDAO = await XCDevice.daoWithAgentId(agentId);
    let imeiDAO = await deviceDAO.findAll({
        'limit':countPerpage,
        'offset': countPerpage * (currPage - 1)
    }, {
        "attributes": ['imei']
    });
    if (!imeiDAO) {
        throw XC_BIZ_ERROR_TYPE.NOT_EXIST_DEVICE;
    }
    return imeiDAO;
}


module.exports = {
    XCDevice,
}