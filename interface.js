

/** 注册接口 */
function register(userInfo){
    /**数据库写入用户信息，redis写入用户状态 */
        /**
         * req:{userInfo:{}}
         * res:{code:0/1}
         */
    }
    
    /** 认证*/
    function authenticate(name, ID, objectId){
        /**
         * req:{userInfo:{objectId:"",name:"",ID:""}}
         * res:{code:0/1,data:{}}
         */
    }
    
    /**押金缴纳 */
    function deposit(objectId, chanel){
        /**
         * req:{objectId:"",chanel:""}
         * res:{code:0/1,charge:{Ping++支付}}
         */
    }
    
    /**登录 */
    function login(objectId){
        /**
         * req:{objectId:""}
         * res:{code:0/1,userInfo:{}}
         */
    }
    
    /**用户状态查询 */
    function getUserStatus(objectId){
        /**
         * res: status //用户状态
         */
    }
    
    /**预约*/
    function reservation(objectId, eBikeInfo){
        /**
         * req:{objectId:"",eBikeInfo:{}}
         * res:{code:0/1,result:true/false}
         */
    }
    
    /**取消预约 */
    function cancelReservation(objectId, eBikeInfo){
        /**
         * req:{objectId:"",eBikeInfo:{}}
         * res:{code:0/1,result:true/false}
         */
    }
    
    /**开锁 */
    function unlock(objectId, eBikeInfo){
        /**
         * 根据用户状态确定开锁逻辑：普通开锁、预约开锁、临时停车开锁
         * req:{objectId:"",eBikeInfo:{}}
         * res:{code:0/1,data:{result:Code,msg：""}}
         * 错误码：
         *  1：未支付
         *  2：与预约车辆不符
         */
    }
    
    /**获取车辆信息 */
    function geteBikeInfo(carId){
        /**
         * res:{code:0/1,result:{//车辆状态}}
         */
    }                
    
    /**临时停车 */
    function tempParking(objectId,eBikeInfo){
        /**
         * req:{objectId:"",eBikeInfo:{}}
         * res:{code:0/1,result:true/false}
         */
    }
    
    /**结束行程 */
    function lock(objectId,eBikeInfo){
        /**
         * req:{objectId:"",eBikeInfo:{}}
         * res:{code:0/1,data:{result:Code,charge:{},info:{}}}
         * 
         * Code:
         *  1:不在指定电子围栏区域
         *  2:锁车失败
         */
    }
    
    /**获取用户未支付订单信息 */
    function getChargeInfo(objectId){
        //保留，看用户状态的存储结构
    }
    
    /**报修 */
    function repair(objectId, eBikeInfo, repairInfo){
        /**
         * req:{objectId:"",eBikeInfo:{},repairInfo:{}}
         * res:{code:0/1}
         */
    }
    
    
    
    
    
    
    
    
    /**我的钱包 */
    function getMyWallet(objectId){
        /**
         * req:{objectId:""
         * res:{code:0/1,wallet:{deposit:"",balance:""}}
         */
    }
    
    /**充值 */
    function recharge(objectId, money){
        /**
         * req:{objectId:"",money:""}
         * res:{code:0/1,balance:""}
         */
    }
    
    /**账户明细 */
    function accountDetail(objectId){
        /**
         * req:{objectId:""}
         * res:{code:0/1,details:{}}
         */
    }
    
    /**行程记录 */
    function itinerary(objectId){
        /**
         * req:{objectId:""}
         * res:{code:0/1,itinerary:{}}
         */
    }
    
    /**客服 */
    function customerService(){
        //问题反馈，拨打客服电话
    }
    
    /**帮助 */
    
    /**优惠券信息 */
    
    /**消息 */
    
    /**获取车辆位置信息 */
    function eBikeLocation(location){
        /**
         * req:{location:{}}
         * res:{code:0/1,eBikeLocation:{}}
         */
    }
    
    /**获取电子围栏信息 */
    function GOFInfo(location){
        /**
         * req:{location:{}}
         * res:{code:0/1,GOFInfos:{}}
         */
    }
    
    /**信用积分 */
    function credit(objectId){
        /**
         * req:{objectId:""}
         * res:{}
         */
    }
    
    /**更换手机号 */
    function changePhoneNum(objectId,newPhoneNum){
        /**
         * 
         */
    }  
    
    /**费用计算 */ 
    
    
    
    
    
    
    