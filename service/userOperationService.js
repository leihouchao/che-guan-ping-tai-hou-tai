/**
 * 用户-车操作类
 */

const DEVICE_STATUS = ['null','READY','RIDING','BROKEN','BOOKING','OFFLINE','ALARM'];
const USER_STATUS = ['null', 'SIGN_UP','AUTHED','READY','BOOKING','LEAVING','RIDING','TO_PAY'];
const XCEBike = require('./../utils/XCEBikeUtils').XCEBike;
// const XC_EBIKE_USER_STATE = XCEBike.XCEBikeUser.XC_EBIKE_USER_STATE;
// const XC_EBIKE_DEVICE_STATE = XCEBike.XCEBikeDevice.XC_EBIKE_DEVICE_STATE;
const XCEBikeUser = XCEBike.XCEBikeUser;
const XCEBikeDevice = XCEBike.XCEBikeDevice;

const logger = require('./../utils/XCLogUtil').logger;
const XCError = require('./../utils/XCErrorUtil').XCError;
const XCResult = require('./../utils/XCResultUtil').XCResult;
const ERR_TYPE = require('./../utils/XCErrorUtil').XC_BIZ_ERROR_TYPE;
const deviceService = require('./deviceService').deviceService;

const XC_EBIKE_USER_STATE = {
    SIGN_UP: '1',
    AUTHED: '2',
    READY: '3',
    BOOKING: '4',
    LEAVING: '5',
    RIDING: '6',
    TO_PAY: '7',
}
const XC_EBIKE_DEVICE_STATE = {
    READY: '1',
    RIDING: '2',
    BROKEN: '3',
    BOOKING: '4',
    OFFLINE: '5',
    ALARM: '6',
};

class userOperationService {
    constructor(objectId, carId){
        //imei, carID 转换 
        this.imei = carId;
        this.objectId = objectId;
        this.deviceService = new deviceService(carId);
        this.user = new XCEBikeUser(objectId);
        this.device = new XCEBikeDevice({imei:this.imei});
    }

    /**
     * 用户开锁
     */
    async unlock(){
        let cmd = {
            imei: this.imei,
            cmd:{
                c:33
            }
        }
        let deviceStatus = await this.device.getState();
        let userStatus = await this.user.getState();
        switch(userStatus){
            case XC_EBIKE_USER_STATE.READY:
                if(deviceStatus === XC_EBIKE_DEVICE_STATE.READY){//车辆可用
                    //正常解锁，发送开锁命令
                    let obj = await this.deviceService.sendCMD(cmd);
                    logger.info('sendCMD obj: ', obj);
                    await this.device.setState(XC_EBIKE_DEVICE_STATE.RIDING);
                    await this.device.setRidingUsrId(this.objectId);
                    await this.user.setState(XC_EBIKE_USER_STATE.RIDING);
                    await this.user.setRidingDevice(this.imei);
                    logger.info('userOperationService.js unlock: ' + 'successful');
                    return new XCResult(true, '开锁成功');
                }else{
                    //该车辆不可用
                    logger.info('userOperationService.js unlock: ' + 'failed, deviceStatus: '+ DEVICE_STATUS[deviceStatus]);
                    return new XCResult(false, '开锁失败');
                }
                break;
            case XC_EBIKE_USER_STATE.BOOKING:
                let bookingUser = await this.device.bookingUsrId();
                if(bookingUser !== this.objectId){//与预约车辆不符
                    logger.info('userOperationService.js reservation: ' + 'failed, deviceStatus： ' + DEVICE_STATUS[deviceStatus]);
                    return new XCResult(false, '与预约车辆不符，开锁失败');
                }else if(deviceStatus !== XC_EBIKE_DEVICE_STATE.BOOKING){//车辆状态异常
                    logger.info('userOperationService.js reservation: ' + 'failed, deviceStatus： ' + DEVICE_STATUS[deviceStatus]);
                    return new XCResult(false, '车辆状态异常，开锁失败');
                }else{//符合开锁条件
                    //发送开锁命令
                    await this.deviceService.sendCMD(cmd);
                    // await this.device.delBookingInfo();
                    await this.device.setState(XC_EBIKE_DEVICE_STATE.RIDING);
                    await this.user.setState(XC_EBIKE_USER_STATE.RIDING);
                    await this.user.delBookingInfo();
                    logger.info('reservation unlock: ' + 'successful');
                    return new XCResult(true, '预约车辆开锁成功');
                }
                break;
            case XC_EBIKE_USER_STATE.LEAVING:
                //临时停车解锁，查询用户的临时停车信息，与现有的信息进行匹配
                //若信息匹配，则发送开锁命令，改变用户的状态
                let imei = await this.user.ridingDevice();
                if(imei){
                    await this.deviceService.sendCMD(cmd);
                    await this.user.setState(XC_EBIKE_USER_STATE.RIDING);
                    return new XCResult(true, '临时停车开锁成功');
                }else{
                    logger.info('userOperationService.js tempParking unlock: ' + 'failed, deviceStatus： ' + DEVICE_STATUS[deviceStatus]);
                    return new XCResult(false, '临死停车开锁失败');
                }               
                break;
            default:
                return new XCResult(false, 'Fail');
                break;
        }
    }

    /**
     * 用户结束行程，落锁
     */
    async lock(){
        let cmd = {
            imei: this.imei,
            cmd:{
                c:4
            }
        }
        let deviceStatus = this.device.getState();
        let userStatus = this.user.getState();

        //TODO:判断用户是否可以进行结束行程操作，车辆是否在指定的停车点

        //发送锁车命令，修改用户和车辆的状态；进行费用计算，将生成行程-订单信息存入数据库，
        //返回用户本次骑行的账单信息
        await this.deviceService.sendCMD(cmd);
        await this.user.setState(XC_EBIKE_USER_STATE.TO_PAY);
        await this.device.setState(XC_EBIKE_DEVICE_STATE.READY);
        let bill = {
            start: "华中科技大学启明学院",
            end: "华中科技大学西十二教学楼",
            time: "07:40-08:00",
            totalTime: "20",
            miles: "2.2",
            charge: "3.5"
        }
        return new XCResult(true, bill);
    }

    /**
     * 预约车辆
     */
    async reserve(){
        if(user.getState() === XC_EBIKE_USER_STATE.READY){//判断用户状态
            if(device.getState() === XC_EBIKE_DEVICE_STATE.READY){//判断车辆状态
                await this.user.setState(XC_EBIKE_USER_STATE.BOOKING);
                await this.user.setBookingImei(this.imei);
                await this.device.setState(XC_EBIKE_DEVICE_STATE.BOOKING, this.objectId);
                await this.device.setBookingUsrId(this.objectId);
                return new XCResult(true, '预约成功');
            }
        }else{
            return new XCResult(false, '预约失败');
        }
    }

    /**
     * 取消预约
     */
    async cancel(){
        this.user.delBookingInfo();
        //改变用户和车辆的状态
        this.user.setState(agentId, XC_EBIKE_USER_STATE.READY);
        this.devcie.setState(XC_EBIKE_DEVICE_STATE.READY);
        return new XCResult(true, '取消预约成功');
    }

    /**
     * 临时停车
     */
    tempPrking(){
        this.deviceService.sendCMD('cmd'); //发送锁车命令
        this.user.setState(XC_EBIKE_USER_STATE.LEAVING);
        return new XCResult(true, '临时停车成功');
    }
}


module.exports = {
    userOperationService,
}
