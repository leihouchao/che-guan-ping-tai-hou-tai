/**
 * 用户类，封装用户信息相关的操作
 */

const DEVICE_STATUS = ['null','READY','RIDING','BROKEN','BOOKING','OFFLINE','ALARM'];
const USER_STATUS = ['null', 'SIGN_UP','AUTHED','READY','BOOKING','LEAVING','RIDING','TO_PAY'];
const XCEBike = require('./../utils/XCEBikeUtils').XCEBike;
const XC_EBIKE_USER_STATE = XCEBike.XCEBikeUser.XC_EBIKE_USER_STATE;
const XC_EBIKE_DEVICE_STATE = XCEBike.XCEBikeDevice.XC_EBIKE_DEVICE_STATE;
const XCEBikeUser = XCEBike.XCEBikeUser;
const XCEBikeDevice = XCEBike.XCEBikeDevice;

const logger = require('./../utils/XCLogUtil');
const XCError = require('./../utils/XCErrorUtil').XCError;
const XCResult = require('./../utils/XCResultUtil').XCResult;
const ERR_TYPE = require('./../utils/XCErrorUtil').XC_BIZ_ERROR_TYPE;
const deviceService = require('./deviceService').devDeviceServiceice;
const zmxyService = require('./zmxyService');

class User{
    constructor(objectId){
        this.objectId = objectId;
        this.user = new XCEBikeUser(objectId);
    }

    /**
     * 用户注册
     */
    async register(tel){
        this.user.setState(XC_EBIKE_USER_STATE.SIGN_UP);
        let userModel = await this.user.getUserDAO();
        userModel.create({
            id: this.objectId,
            phone: tel
        });
        return new XCResult(true, 'success');
    }

    /**
     * 用户实名认证
     */
    async authenticate(userInfo){
        let response = await zmxyService.authenticate(userInfo); 
        let userModel = await this.user.getUserDAO();
        let userInfoJson = {
            name: userInfo.name,
            cert_no: userInfo.cert_no,
            authInfo: response.result
        };
        userModel.update({
            authed: response.result.success,
            personInfo: JSON.stringify(userInfoJson)
        }, {where:{
            id: this.objectId
        }});
        if(response.result.success){
            this.user.setState(XC_EBIKE_USER_STATE.READY);
        }
        return response.result;
    }

    /**
    * 更换手机号
    */
    changePhoneNum(newPhoneNum){
        
    }

    /**
    * 获取用户信息
    */
    async getUserInfo(){
        await this.user.loadAllInfoSlow();
        return this.user;
    }

    /**
     * 用户消费情况
     */
    costCalculate(){
        let cost = 0;
        let time = 1;
        let miles = 1000;
        if(miles < 3000){
            cost = 3 + COST_MINUTE * time;
        }
        if(miles > 3000){
            let out_mile = Math.ceil(miles - 3000);
            cost = out_mile * COST_MILE + time * COST_MINUTE;
        }
        return cost;
    }
}



module.exports = {
    User,
}