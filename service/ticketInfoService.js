/**
 * 工单信息操作
 */
const XCEBike = require('../utils/XCEBikeUtils').XCEBike;
let agentDeviceInfoService = require('./../service/agentDeviceInfoService').agentDeviceInfoService;

let FIX_STATE = XCEBike.XCEBikeTicket.FIX_STATE;
let FIX_TYPE = XCEBike.XCEBikeTicket.FIX_TYPE;
let XCEBikeFixTicket = XCEBike.XCEBikeFixTicket;
let XCEBikeAlarmTicket = XCEBike.XCEBikeAlarmTicket;
const TICKET_TYPE = {
    FIX: 'FIX',
    ALARM: 'ALARM'
}

class ticketInfo {
    constructor(agentId) {
        this._agentId = agentId;
    }
    static get TICKET_TYPE() {
        return TICKET_TYPE;
    }

    async getSingleTicketInfo(ticketNo, type) {
        let ticket = null;
        if (type == TICKET_TYPE.FIX) {
            ticket = new XCEBikeFixTicket(this._agentId);
        } else if (type == TICKET_TYPE.ALARM) {
            ticket = new XCEBikeAlarmTicket(this._agentId);
        }
        let ticketInfo = await ticket.ticketInfoInDB(ticketNo);
        return ticketInfo[0].dataValues;
    }
    //获取异常工单信息
    async getAlarmTicketInfo(pageSize, pageNumber) {
        let alarmTicket = new XCEBikeAlarmTicket(this._agentId);
        let alarmTicketInfo = await alarmTicket.ticketList(Number(pageSize), Number(pageNumber));
        let agentDeviceInfo = new agentDeviceInfoService(this._agentId);
        let alarmList = [];
        for (let value of alarmTicketInfo) {
            let imeiInfo = await agentDeviceInfo.getAlarmImeiInfo(value.dataValues.imei);
            let json = {
                device: imeiInfo,
                ticket: value.dataValues
            }
            alarmList.push(json);
        }
        return alarmList;
    }
    //获取维修工单信息
    async getFixTicketInfo(pageSize, pageNumber) {
        let fixTicket = new XCEBikeFixTicket(this._agentId);
        let fixTicketInfo = await fixTicket.ticketList(Number(pageSize), Number(pageNumber));
        let agentDeviceInfo = new agentDeviceInfoService(this._agentId);
        let fixList = [];
        for (let value of fixTicketInfo) {
            let imeiInfo = await agentDeviceInfo.getfixImeiInfo(value.dataValues.imei);
            let json = {
                device: imeiInfo,
                ticket: value.dataValues
            }
            fixList.push(json);
        }
        return fixList;
    }
    /**上传异常工单信息
     * @param {*} json
     * imei, ebikeNo, alarmType, alarmState
     */
    async postAlarmTicketInfo(imei, ebikeNo, alarmType, alarmState) {
        let alarmTicket = new XCEBikeAlarmTicket(this._agentId);
        let alarmJson = {
            imei: imei,
            ebikeNo: ebikeNo,
            type: alarmType,
            state: alarmState
        };
        await alarmTicket.create(alarmJson);
    }

    /**上传维修工单信息
     * @param {*} json
     * 必须:
     * imei, ebikeNo, fixType, fixState, reportUsrId
     */
    async postFixTicketInfo(imei, ebikeNo, fixType, fixState, reportUsrId) {
        let fixTicket = new XCEBikeFixTicket(this._agentId);
        let fixJson = {
            imei: imei,
            ebikeNo: ebikeNo,
            type: fixType,
            state: fixState,
            reportUsrId: reportUsrId
        };
        await fixTicket.create(fixJson);
    }
    //修改异常工单信息--添加维修人员信息
    async updateAlarmTicketInfo(ticketNo, fixManId) {
        let alarmTicket = new XCEBikeAlarmTicket(this._agentId);
        let updateInfo = await XCEBikeAlarmTicket.updateInfo(ticketNo, fixManId);
        return updateInfo;   
    }
    //修改维修工单信息--添加维修人员信息
    async updateFixTicketInfo(ticketNo, fixManId) {
        let fixTicket = new XCEBikeFixTicket(this._agentId);
        let updateInfo = await fixTicket.updateInfo(ticketNo, fixManId);
        return updateInfo;     
    }

    async closeTicket(ticketNo, type) {
        let ticket = null;
        if (type == TICKET_TYPE.FIX) {
            ticket = new XCEBikeFixTicket(this._agentId);
        } else if (type == TICKET_TYPE.ALARM) {
            ticket = new XCEBikeAlarmTicket(this._agentId);
        }

        let handledTicket = await ticket.handled(ticketNo);
        return handledTicket;
    }
}

module.exports = {
    ticketInfo
}