/**
 * 电子围栏信息的操作
 */
const XCEBike = require('../utils/XCEBikeUtils').XCEBike;
let XCEBikeDevice = XCEBike.XCEBikeDevice;
let XCEBikeUser = XCEBike.XCEBikeUser;
let XCEBikeUserOrder= XCEBike.XCEBikeUserOrder;
let electricFenceType = {
    travelRange:'1',//行驶范围
    rentingArea:'2' //租停区
};

class electricFenceService{
    static get electricFenceType() {
        return electricFenceType;
    }

    constructor(agentId) {
        this._agentId = agentId;
    }

    /**
     * 新建一条电子围栏
     * @param {arr,arr保存存储的数据} arr 
     * @param {string,type区分形式范围和租停区} type 
     */
    async create(arr, type) {
        //根据agentId和type获取到实例
    }

    /**
     * 根据id删除电子围栏，只置标志位，sequelize 开启 paranoid保存删除时间
     * @param {int} id 
     * @param {string,type区分形式范围和租停区} type 
     */
    async delete(id, type) {
        //根据agentId和type获取到实例
    }

    /**
     * 根据type获取所有的电子围栏
     * @param {string} type 
     */
    async getAllFence(type) {

    }

    /**
     * 
     * @param {更新json里面的所有数据} json 
     * @param {*} type 
     */

    async updateFence(json, type) {

    }

}

module.exports = {
    electricFenceService
}