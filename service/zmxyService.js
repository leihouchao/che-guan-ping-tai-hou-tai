/**
 * 芝麻认证服务
 */
const zmxyClient = require('zmxy').default;
const XCError = require('./../utils/XCErrorUtil').XCError;
const XCResult = require('./../utils/XCResultUtil').XCResult;
const ERR_TYPE = require('./../utils/XCErrorUtil').XC_BIZ_ERROR_TYPE;

const fs = require('fs');
const zmxy = new zmxyClient({
    appId: '300000923',
    appPrivateKey: fs.readFileSync('./cert/app_private_key.pem'),
    zmxyPublicKey: fs.readFileSync('./cert/zmxy_public_key.pem'),
});

async function authenticate(userInfo){
    let params = {
        name: userInfo.name,
        cert_no: userInfo.cert_no
    };
    return await zmxy.verifyIvs(params);
}

module.exports = {
    authenticate,
}