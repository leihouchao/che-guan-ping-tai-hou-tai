/**
 * 获取代理商设备信息
 */

const XCDevice = require('./../do/XCDevice').XCDevice;

const XCEBike = require('./../utils/XCEBikeUtils').XCEBike;
const XCEBikeDevice = XCEBike.XCEBikeDevice;
const DEVICE_STATE = XCEBike.XCEBikeDevice.XC_EBIKE_DEVICE_STATE;

class agentDeviceInfoService {
    constructor(agentId) {
        this._agentId = agentId;
    }

    async getAllImeiByPageSize(countPerpage, currPage) {
        let deviceList = await XCDevice.getImeiByPageSize(this._agentId, Number(countPerpage), Number(currPage));
        let imeiList = [];
        for (let value of deviceList) {
            let imei = value.dataValues.imei;
            let deviceInfo = await XCEBikeDevice.instanceFromRedis(imei);
            imeiList.push({device:deviceInfo});
        }
        return imeiList;
    }

    async getNormalImeiInfo(countPerpage, currPage) {
        let deviceList = await XCDevice.getAgentAllImei(this._agentId);
        let imeiList = [];
        countPerpage = Number(countPerpage);
        currPage = Number(currPage);
        let normalSum = 0;
        let storeNormalSum = 0;
        for (let value of deviceList) {
            let imei = value.dataValues.imei;
            let deviceInfo = await XCEBikeDevice.instanceFromRedis(imei);
            let state = Number(deviceInfo.state);
            if (state == DEVICE_STATE.READY || state == DEVICE_STATE.RIDING || state == DEVICE_STATE.BOOKING) {
                normalSum += 1;
                if (storeNormalSum < countPerpage && (normalSum > countPerpage * (currPage - 1)) && normalSum <= countPerpage * currPage) {
                    imeiList.push({device: deviceInfo});
                    storeNormalSum += 1;
                }

                if (normalSum > countPerpage * currPage)
                    break;
            }
        }
        return imeiList;
    }

    async getAlarmImeiInfo(imei) {
        let deviceInfo = await XCEBikeDevice.instanceFromRedis(imei);
        return deviceInfo;
    }


    async getfixImeiInfo(imei) {
        let fixImeiInfo = await XCEBikeDevice.instanceFromRedis(imei);
        return fixImeiInfo;
    }

    async getOffLineImeiInfo(countPerpage, currPage) {
        countPerpage = Number(countPerpage);
        currPage = Number(currPage);
        let deviceList = await XCDevice.getAgentAllImei(this._agentId);
        let imeiList = [];
        let normalSum = 0;
        let storeNormalSum = 0;
        for (let value of deviceList) {
            let imei = value.dataValues.imei;
            let deviceInfo = await XCEBikeDevice.instanceFromRedis(imei);
            if (Number(deviceInfo.state) == DEVICE_STATE.OFFLINE) {
                normalSum += 1;
                if (storeNormalSum < countPerpage && (normalSum > countPerpage * (currPage - 1)) && normalSum <= countPerpage * currPage) {
                    imeiList.push(deviceInfo);
                    storeNormalSum += 1;
                }

                if (normalSum > countPerpage * currPage)
                    break;
            }
        }
        return imeiList;
    }

}

module.exports = {
    agentDeviceInfoService
}