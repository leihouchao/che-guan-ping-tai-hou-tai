/**
 * 车辆类，封装车辆相关信息的操作
 */

const XCEBike = require('./../utils/XCEBikeUtils').XCEBike;
const XCEBikeDevice = XCEBike.XCEBikeDevice;
const XC_EBIKE_DEVICE_STATE = XCEBike.XCEBikeDevice.XC_EBIKE_DEVICE_STATE;

const client = require('./../utils/XCRedisUtil').client;
const logger = require('./../utils/XCLogUtil').logger;
const config = require('./../config.json');
const restify = require('restify-clients');
const url= require('url');

const XCError = require('./../utils/XCErrorUtil').XCError;
const XCResult = require('./../utils/XCResultUtil').XCResult;
const ERR_TYPE = require('./../utils/XCErrorUtil').XC_BIZ_ERROR_TYPE;

class deviceService{

    constructor(carId){
        this.carId = carId;
        this.imei = carId;
    }

    /**
     * 获取车辆位置信息
     */
    eBikeLocation(userLocation){
        /**
         * req:{location:{}}
         * res:{code:0/1,eBikeLocation:{}}
         */
    }

    /**
     * 获取车辆状态信息
     */
    geteBikeInfo(){

    }

    /**
     * 发送命令字，开锁、上锁
     */
    async sendCMD(cmd){
        return new Promise((resolve, reject) => {
            client.get(cmd.imei, (getErr, getRes) => {
                if (getErr) {
                    logger.error('deviceService.js transparent transmission '+ cmd.imei +': connect error:'+ getErr);
                    reject(getErr);
                } else if(!getRes) {
                    logger.info('deviceService.js transparent transmission '+ cmd.imei+': error:Data in the redis server is empty,getRes empty,imei');
                    let JSONClient = restify.createJsonClient({
                        url: 'http://' + config.device_http_options.host
                  });
                  
                JSONClient.post(config.device_http_options.path, cmd, (error, request, response, obj) => {
                    if(error !== null){
                        logger.error(error.message);
                        reject(error);
                    }else{
                        loger.info('deviceService.js send cmd successful, cmd: ', cmd);
                        resolve(obj);
                    }});
                }else{
                    let Url = url.parse('http://' + getRes);
                    logger.info('deviceService.js ready into the http connect:', config.device_http_options.path);
          
                    let JSONClient = restify.createJsonClient({
                        url: 'http://' + getRes
                    });
          
                    JSONClient.post(config.device_http_options.path, cmd,(error, request, response, obj) => {
                        if(error){
                            reject(error);
                        }else{
                            resolve(obj);
                        }
                    });
                }
            });
        })
    }
}


module.exports = {
    deviceService,
}