/**
 * created on 2017-11-10 by Shenghui Wu
 * ping++支付接入
 */

const pingppConfig = require('./../config.json').pingpp;
var pingpp = require('pingpp')(pingppConfig.apiKey);

class PingppService {
    constructor() {

    }

    static async createCharge(chargeInfo) {
        return new Promise(function(resolve, reject) {
            pingpp.charges.create({
                subject: chargeInfo.subject,
                body: chargeInfo.body,
                amount: chargeInfo.amount,
                order_no: chargeInfo.order_no,
                channel: chargeInfo.channel,
                currency: "cny",
                client_ip: chargeInfo.client_ip,
                app: {id: pingppConfig.appId},
                time_expire: chargeInfo.time_expire || null,
                metadata: chargeInfo.metadata
            }, function(err, charge) {
                if (err) {
                    return reject(err);
                } else {
                    return resolve(charge);
                }
            })
        })
        .then(charge => charge)
        .catch(err => err);
    }

    static async retrieveCharge(order_id) {
        return new Promise(function(resolve, reject) {
            pingpp.charges.retrieve(order_id, function(err, charge) {
                if (err) {
                    return reject(err);
                } else {
                    return resolve(charge);
                }
            })
        })
        .then(charge => charge)
        .catch(err => err);
    }
}


/**
 * created on 20171108
 * 获取用户历史支付信息
 */

function getChargeInfo(objectId){
    
}
    


module.exports = {
    PingppService,
    getChargeInfo,
}