/**
 * 平台首页信息的操作
 */
const XCEBike = require('../utils/XCEBikeUtils').XCEBike;
let XCEBikeDevice = XCEBike.XCEBikeDevice;
let XCEBikeUser = XCEBike.XCEBikeUser;
let XCEBikeUserOrder= XCEBike.XCEBikeUserOrder;
const moment = require('moment');


class platFormMainPageInfo{
    constructor(agentId) {
        this._agentId = agentId;
    }
    //获取首页设备信息，只有各项总数
    //设备总数，正常数，维修设备数，异常设备数，过期离线设备数
    async getDeviceSum() {
        let arr = [];
        let deviceSum = await XCEBikeDevice.deviceCount(this._agentId);
        let deviceAlarmSum =  await XCEBikeDevice.deviceAlarmCount(this._agentId);
        let deviceBrokenSum = await XCEBikeDevice.deviceBrokenCount(this._agentId);
        let deviceOffLineSum = await XCEBikeDevice.deviceOfflineCount(this._agentId);
        let deviceNormalSum = deviceSum - deviceAlarmSum - deviceBrokenSum - deviceOffLineSum;
        arr.push({
            'x':'正常设备数',
            'y':deviceNormalSum
        });      
        arr.push({
            'x':'异常设备数',
            'y':deviceAlarmSum
        });       
        arr.push({
            'x':'维修设备数',
            'y':deviceBrokenSum
        });        
        arr.push({
            'x':'离线设备数',
            'y':deviceOffLineSum
        });
        return arr;
    }

    async getTotalDeviceSum() {
        let arr = [];
        let deviceSum = await XCEBikeDevice.deviceCount(this._agentId);
        arr.push({
            'x':'总设备数',
            'deviceSum':deviceSum
        });
        return arr;
    }
    async getOffLineSum() {
        let arr= [];
        let deviceOffLineSum = await XCEBikeDevice.deviceOfflineCount(this._agentId);
        arr.push({
            'x':'离线设备数',
            'deviceOffLineSum':deviceOffLineSum
        });
        return arr; 
    }

    async getAlarmSum() {
        let arr = [];
        let deviceAlarmSum =  await XCEBikeDevice.deviceAlarmCount(this._agentId);
        arr.push({
            'x':'异常设备数',
            'deviceAlarmSum':deviceAlarmSum
        });       
        return arr;
    }
    async getNormalSum() {
        let arr = [];
        let deviceSum = await XCEBikeDevice.deviceCount(this._agentId);
        let deviceAlarmSum =  await XCEBikeDevice.deviceAlarmCount(this._agentId);
        let deviceBrokenSum = await XCEBikeDevice.deviceBrokenCount(this._agentId);
        let deviceOffLineSum = await XCEBikeDevice.deviceOfflineCount(this._agentId);
        let deviceNormalSum = deviceSum - deviceAlarmSum - deviceBrokenSum - deviceOffLineSum;
        arr.push({
            'x':'正常设备数',
            'deviceNormalSum':deviceNormalSum
        }); 
        return arr;
    }
    async getBrokenSum() {
        let arr = [];
        let deviceBrokenSum = await XCEBikeDevice.deviceBrokenCount(this._agentId);
        arr.push({
            'x':'维修设备数',
            'brokenSum':deviceBrokenSum
        });  
        return arr;
    }

    //获取总用户量，有押金用户量
    async getUserSum() {
        let userSum = await XCEBikeUser.userCount(this._agentId);
        let dUsrSum = await XCEBikeUser.depositUserCount(this._agentId);
        let json = {
            'aims': userSum,
            'actual': dUsrSum
        }
        return json;
    }

    //获取流水量：总流水、今日流水
    async getTransactionSum() {
        let incomeSum = await XCEBikeUserOrder.income(this._agentId);
        let incomeToday = await XCEBikeUserOrder.incomeToday(this._agentId);
        let arr = [];
        arr.push({
            'x': moment().utcOffset(-8).subtract(1, "months").format('YYYY-MM-DD HH:mm:ss'),
            'y': incomeSum,
            's':1
        });
        arr.push({
            'x':moment().utcOffset(-8).subtract(1, "months").format('YYYY-MM-DD HH:mm:ss'),
            'y':incomeToday,
            's':2
        });

        arr.push({
            'x': moment().utcOffset(-8).format('YYYY-MM-DD HH:mm:ss'),
            'y': incomeSum,
            's':1
        });
        arr.push({
            'x':moment().utcOffset(-8).format('YYYY-MM-DD HH:mm:ss'),
            'y':incomeToday,
            's':2
        })
        return arr;
    }

    //获取订单数量，总订单量，今日订单量，未结订单量
    async getOrderSum() {
        let orderSum = await XCEBikeUserOrder.orderCount(this._agentId);
        let orderTodaySum = await XCEBikeUserOrder.orderCountToday(this._agentId);
        let unPaidSum = 99;
        let arr = [];
        arr.push({
            'x': moment().utcOffset(-8).subtract(2, "months").format('YYYY-MM-DD HH:mm:ss'),
            'y': orderSum,
            's':1
        });
        arr.push({
            'x':moment().utcOffset(-8).subtract(2, "months").format('YYYY-MM-DD HH:mm:ss'),
            'y':orderTodaySum,
            's':2
        });
        arr.push({
            'x':moment().utcOffset(-8).subtract(2, "months").format('YYYY-MM-DD HH:mm:ss'),
            'y':unPaidSum,
            's':3
        });
        arr.push({
            'x': moment().utcOffset(-8).subtract(1, "months").format('YYYY-MM-DD HH:mm:ss'),
            'y': orderSum,
            's':1
        });
        arr.push({
            'x':moment().utcOffset(-8).subtract(1, "months").format('YYYY-MM-DD HH:mm:ss'),
            'y':orderTodaySum,
            's':2
        });
        arr.push({
            'x':moment().utcOffset(-8).subtract(1, "months").format('YYYY-MM-DD HH:mm:ss'),
            'y':unPaidSum,
            's':3
        });
        arr.push({
            'x': moment().utcOffset(-8).format('YYYY-MM-DD HH:mm:ss'),
            'y': orderSum,
            's':1
        });
        arr.push({
            'x': moment().utcOffset(-8).format('YYYY-MM-DD HH:mm:ss'),
            'y':orderTodaySum,
            's':2
        });
        arr.push({
            'x': moment().utcOffset(-8).format('YYYY-MM-DD HH:mm:ss'),
            'y':unPaidSum,
            's':3
        });
        return arr;
    }

}

module.exports = {
    platFormMainPageInfo
}