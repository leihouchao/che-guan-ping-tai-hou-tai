/**
 * created by dingyuan on 20171113
 */

const logger = require('./../utils/XCLogUtil').log;
const restify = require('restify-clients');


async function walkingNavigate(origin, destination){

    let response = {};
    /*高德步行导航接口*/
    let amapUrl = 'http://restapi.amap.com/v3/direction/walking?origin=' 
                + origin.lon + ',' + origin.lat
                + '&destination=' + destination.lon + ',' + destination.lat
                + '&key=c14eb93def053bf0ab1f3669e7371d04';
    let JSONClient = restify.createJsonClient({
        url: amapUrl
    });

    return new Promise((resolve, reject) => {
        JSONClient.get('', (err, req, res, obj) => {
            if(err){
                reject(err);
                logger.error(err);
            }
            if(obj.status !== '1'){
                response.code = 1;
                response.info = obj.info;
                response.infocode= obj.infocode;
                resolve(response);
            }else{
                let steps = obj.route.paths[0].steps;
                let result = [];
        
                for(let step of steps){
                    let polyline = step.polyline.split(';');
                    for(let gps of polyline){
                        let temp = {
                            lon: parseFloat(gps.split(',')[0]),
                            lat: parseFloat(gps.split(',')[1])   
                        };
                        result.push(temp);
                    }
                }
                response.code = 0;
                response.info = obj.info;
                response.polyline = result;
            
                resolve(response);
            }
        });
    });
}

async function drivingNavigate(origin, destination){
    
        let response = {};
        /*高德步行导航接口*/
        let amapUrl = 'http://restapi.amap.com/v3/direction/driving?origin=' 
                    + origin.lon + ',' + origin.lat
                    + '&destination=' + destination.lon + ',' + destination.lat
                    + '&key=c14eb93def053bf0ab1f3669e7371d04';
        let JSONClient = restify.createJsonClient({
            url: amapUrl
        });
    
        return new Promise((resolve, reject) => {
            JSONClient.get('', (err, req, res, obj) => {
                if(err){
                    reject(err);
                    logger.error(err);
                }
                if(obj.status !== '1'){
                    response.code = 1;
                    response.info = obj.info;
                    response.infocode= obj.infocode;
                    resolve(response);
                }else{
                    let steps = obj.route.paths[0].steps;
                    let result = [];
            
                    for(let step of steps){
                        let polyline = step.polyline.split(';');
                        for(let gps of polyline){
                            let temp = {
                                lon: parseFloat(gps.split(',')[0]),
                                lat: parseFloat(gps.split(',')[1])   
                            };
                            result.push(temp);
                        }
                    }
                    response.code = 0;
                    response.info = obj.info;
                    response.polyline = result;
                
                    resolve(response);
                }
            });
        });
    }

module.exports = {
    walkingNavigate,
    drivingNavigate,
}
