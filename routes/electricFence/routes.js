const Router = require('restify-router').Router;
const router = new Router();
//electric fence
router.add('/createElectricFence', require('./createElectricFence'));
router.add('/deleteElectricFence', require('./deleteElectricFence'));
router.add('/updateElectricFence', require('./updateElectricFence'));
module.exports = router;