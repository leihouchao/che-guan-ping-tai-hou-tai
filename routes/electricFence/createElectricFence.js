/**
 * Created by shun14 on 17-11-01.
 */
let Utils = require('../../utils/XCUtil');
let existParam = Utils.existParam;

let ErrorUtils = require('../../utils/XCErrorUtil');
let XCError = ErrorUtils.XCError;
let XC_BIZ_ERROR_TYPE = ErrorUtils.XC_BIZ_ERROR_TYPE;

let XCResult = require('../../utils/XCResultUtil').XCResult;
let XCLogger = require('../../utils/XCLogUtil').logger;
let electricFenceService = require('./../../service/electricFenceService').electricFenceService;

const Router = require('restify-router').Router;
const router = new Router();

router.post('', function (req, res, next) {
  res.contentType = 'json';
  if (!existParam(req, 'fenceType') || !existParam(req, 'data')||!existParam(req, 'agentId')) {
    let err = new XCError({}, XC_BIZ_ERROR_TYPE.INPUT_PARAMS_MISS);
    let result = new XCResult(false, err);
    res.send(result);
    return next();
  }

  //
  //获取指定的imei然后获取状态，除去总表，其他的状态可能需要获取所有的imei状态才能获取完整
  let body = req.body;
  let fenceType = body.fenceType;//
  let data = body.data;
  let agentId = body.agentId;

  let electricFence = new electricFenceService(agentId);
  
  (async ()=> {
      //TODO 
    let create = await electricFence.create(data, fenceType);
    res.send(new XCResult(true, create));
    return next();
  })().catch(err => {
    XCLogger.error('err occured:',err);
    res.send(new XCResult(false , new XCError(err, XC_BIZ_ERROR_TYPE.BIZ_ERROR)));
    return next();
  })
  
});


module.exports = router;