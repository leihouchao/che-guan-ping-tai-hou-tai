/**
 * created by dingyuan on 20171107
 */

const Router = require('restify-router').Router;
const router = new Router();

const NGINX_ROUTER = '/ebike';//路径web区分

router.add(NGINX_ROUTER+'/user', require('./user/routes'));
router.add(NGINX_ROUTER+'/device', require('./device/routes'));
router.add(NGINX_ROUTER + '/platform', require('./platform/routes'));
router.add(NGINX_ROUTER+'/electricFence', require('./electricFence/routes'));
module.exports = router;