/**
 * created on 20171108
 * @api {post} /eBike/device/lock lock
 * @apiName lock
 * @apiVersion 1.0.0
 * @apiGroup device
 * @apiDescription 结束行程
 * @apiParam objectId 用户Id
 * @apiParam carId 车辆Id
 * @apiParamExample request:
 * {
 *     "objectId":"",
 *     "carId":""
 * }
 * @apiSuccessExample response:
 * {
 *     "suc":true,
 *     "data":{}
 * }
 */
const Router = require('restify-router').Router;
const router = new Router();

const logger = require('./../../utils/XCLogUtil');
const XCError = require('./../../utils/XCErrorUtil').XCError;
const XCResult = require('./../../utils/XCResultUtil').XCResult;
const ERR_TYPE = require('./../../utils/XCErrorUtil').XC_BIZ_ERROR_TYPE;
const userOperationService = require('./../../service/userOperationService').userOperationService;

router.post('', (req, res, next) => {
    if(!req.body){
        logger.info('no data!');
        res.send(new XCResult(false, new XCError("request no body!", ERR_TYPE.INPUT_PARAMS_MISS)));
        return next(); 
    }

    let objectId = req.body.objectId;
    let carId = req.body.carId;
    let userOperation = new userOperationService(objectId, carId);
    userOperation.lock()
    .then(result => {
        res.send(result);
        return next();
    })
    .catch(error => {
        res.send(new XCResult(false, error));
        return next();
    });
});


module.exports = router;