/**
 * created on 20171107
 * @api {get} /eBike/device/eBikeInfo eBikeInfo
 * @apiName eBikeInfo
 * @apiVersion 1.0.0
 * @apiGroup device
 * @apiDescription 获取车辆信息
 * @apiParam carId 车辆ID
 * @apiParamExample request:
 * /eBike/device/eBike/123456
 * @apiSuccessExample response:
 * {
 *     "suc":true,
 *     "data":{}
 * }
 */

const Router = require('restify-router').Router;
const router = new Router();

const logger = require('./../../utils/XCLogUtil');
const XCError = require('./../../utils/XCErrorUtil').XCError;
const XCResult = require('./../../utils/XCResultUtil').XCResult;
const ERR_TYPE = require('./../../utils/XCErrorUtil').XC_BIZ_ERROR_TYPE;

const XCEBike = require('./../../utils/XCEBikeUtils').XCEBike;
const XCEBikeDevice = XCEBike.XCEBikeDevice;

router.get('/:carId', (req, res, next) => {
    if(!req.params.hasOwnProperty('carId')){
        logger.info('no data!');
        res.send(new XCResult(false, new XCError("request no carId!", ERR_TYPE.INPUT_PARAMS_MISS)));
        return next();
    }
    let carId = req.params.carId;
    //根据carId获取imei
    let imei = carId;
    //根据imei获取车辆信息
    XCEBikeDevice.instanceFromRedis(imei)
    .then(device => {
        res.send(new XCResult(true, device));
        return next(); 
    })
    .catch(error => {
        res.send(new XCResult(false, error));
        return next(); 
    });
});


module.exports = router;