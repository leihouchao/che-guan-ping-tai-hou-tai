/**
 * created on 20171107
 * @api {get} /eBike/device/ridingInfo ridingInfo
 * @apiName ridingInfo
 * @apiVersion 1.0.0
 * @apiGroup device
 * @apiDescription 获取用户骑行的实时信息
 * @apiParam objectId 用户Id 
 * @apiParam carId 车辆Id
 * @apiParamExample request:
 * {
 *      "objectId":"",
 *      "carId":""
 * }
 * @apiSuccessExample response:
 * {
 *     "suc":true,
 *     "data":""
 * }
 * 
 */

const Router = require('restify-router').Router;
const router = new Router();

router.post('', (req, res, next) => {
    if(!req.body){
        logger.info('no data!');
        res.send(new XCResult(false, new XCError("request no body!", ERR_TYPE.INPUT_PARAMS_MISS)));
        return next(); 
    }

    let objectId = req.body.objectId;
    let carId = req.body.carId;
    /**
     * 获取设备的实时信息，主要是费用、行驶时间、行驶距离、当前位置
     */

    res.send(new XCResult(true, 'success!'));
    return next(); 
});

module.exports = router;