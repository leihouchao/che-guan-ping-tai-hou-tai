/**
 * created on 20171107
 * @api {post} /eBike/device/unlock unlock
 * @apiName unlock
 * @apiVersion 1.0.0
 * @apiGroup device
 * @apiDescription 锁车
 * @apiParam objectId 用户Id
 * @apiParam carId 车辆Id
 * @apiParamExample request；
 * {
 *     "objectId":"",
 *     "carId":""
 * }
 * @apiSuccessExample response:
 * {
 *     "suc":true,
 *     "data":{}
 * }
 */

const Router = require('restify-router').Router;
const router = new Router();

const logger = require('./../../utils/XCLogUtil').logger;
const XCResult = require('./../../utils/XCResultUtil').XCResult;
const userOperationService = require('./../../service/userOperationService').userOperationService;

router.post('', (req, res, next) => {
    if(!req.body){
        logger.info('no data!');
        res.send(new XCResult(false, new XCError("request no body!", ERR_TYPE.INPUT_PARAMS_MISS)));
        return next(); 
    }

    let objectId = req.body.objectId;
    let carId = req.body.carId;
    //TODO:根据用户和车辆的状态决定是否开锁
    let userOperation = new userOperationService(objectId, carId);
    userOperation.unlock()
    .then(result => {
        res.send(result);
        return next();
    })
    .catch(error => {
        res.send(new XCResult(false, error.message));
        return next();
    });
});

module.exports = router;