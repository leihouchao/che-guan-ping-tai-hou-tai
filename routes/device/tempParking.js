/**
 * created on 20171107
 * @api {post} /eBike/device/tempParking tempParking
 * @apiName tempParking
 * @apiVersion 1.0.0
 * @apiGroup device
 * @apiDescription 临时停车
 * @apiParam objectId 用户Id
 * @apiParam carId 车辆Id
 * @apiParamExample request:
 * {
 *     "objectId":"",
 *     "carId":""
 * }
 * @apiSuccessExample response:
 * {
 *     "suc":true,
 *     "data":{}
 * }
 */

const Router = require('restify-router').Router;
const router = new Router();

const logger = require('./../../utils/XCLogUtil');
const XCError = require('./../../utils/XCErrorUtil').XCError;
const XCResult = require('./../../utils/XCResultUtil').XCResult;
const ERR_TYPE = require('./../../utils/XCErrorUtil').XC_BIZ_ERROR_TYPE;

router.post('', (req, res, next) => {
    if(!req.body){
        logger.info('no data!');
        res.send(new XCResult(false, new XCError("request no body!", ERR_TYPE.INPUT_PARAMS_MISS)));
        return next(); 
    }

    let objectId = req.body.objectId;
    let carId = req.body.carId;
    //TODO:改变用户和车辆的状态，改为临时停车状态
    
    res.send(new XCResult(true, '临时停车成功'));
    return next();
});

module.exports = router;