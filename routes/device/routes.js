/**
 * created by dingyuan on 20171107
 */

const Router = require('restify-router').Router;
const router = new Router();

router.add('/eBikeInfo', require('./eBikeInfo'));
router.add('/tempParking', require('./tempParking'));
router.add('/lock', require('./lock'));
router.add('/ridingInfo', require('./ridingInfo'));
router.add('/unlock', require('./unlock'));

module.exports = router;