/**
 * Created by shun14 on 17-11-01.
 */
let Utils = require('../../utils/XCUtil');
let existParam = Utils.existParam;

let ErrorUtils = require('../../utils/XCErrorUtil');
let XCError = ErrorUtils.XCError;
let XC_BIZ_ERROR_TYPE = ErrorUtils.XC_BIZ_ERROR_TYPE;

let XCResult = require('../../utils/XCResultUtil').XCResult;
let XCLogger = require('../../utils/XCLogUtil').logger;
let agentDeviceInfo = require('./../../service/agentDeviceInfoService').agentDeviceInfoService;

const Router = require('restify-router').Router;
const router = new Router();

router.post('', function (req, res, next) {
  res.contentType = 'json';
  XCLogger.info('POST ',req.url);

  if (!existParam(req, 'pageSize') || !existParam(req, 'pageNumber')||!existParam(req, 'agentId')) {
    let err = new XCError({}, XC_BIZ_ERROR_TYPE.INPUT_PARAMS_MISS);
    let result = new XCResult(false, err);
    res.send(result);
    return next();
  }

  //页面包含大小        pageSize
  //第几个页面          pageNumber
  
  //获取指定的imei然后获取状态，除去总表，其他的状态可能需要获取所有的imei状态才能获取完整
  let body = req.body;
  let pageSize = body.pageSize;
  let pageNumber = body.pageNumber;
  let agentId = body.agentId;

  let agentDevice = new agentDeviceInfo(agentId);
  (async ()=> {
    let imeiInfoList = await agentDevice.getNormalImeiInfo(pageSize, pageNumber);
    res.send(new XCResult(true, imeiInfoList));
    return next();
  })().catch(err => {
    XCLogger.error('err occured:',err);
    res.send(new XCResult(false , new XCError(err, XC_BIZ_ERROR_TYPE.BIZ_ERROR)));
    return next();
  })
  
});


module.exports = router;