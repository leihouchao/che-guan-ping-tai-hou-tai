/**
 * Created by shun14 on 17-11-13.
 */
let Utils = require('../../utils/XCUtil');
let existParam = Utils.existParam;

let ErrorUtils = require('../../utils/XCErrorUtil');
let XCError = ErrorUtils.XCError;
let XC_BIZ_ERROR_TYPE = ErrorUtils.XC_BIZ_ERROR_TYPE;
let ticketInfoService = require('./../../service/ticketInfoService').ticketInfo;

let XCResult = require('../../utils/XCResultUtil').XCResult;
let XCLogger = require('../../utils/XCLogUtil').logger;
let TICKET_TYPE = ticketInfoService.TICKET_TYPE;
const Router = require('restify-router').Router;
const router = new Router();


//修改工单信息，上传维修人员信息
router.post('/updateInfo', function (req, res, next) {
  res.contentType = 'json';
  if (!existParam(req, 'fixManId') || !existParam('agentId') || !existParam('ticketNo')) {
    let err = new XCError({}, XC_BIZ_ERROR_TYPE.INPUT_PARAMS_MISS);
    let result = new XCResult(false, err);
    res.send(result);
    return next();
  }

  let body = req.body;

  let ticketNo = body.ticketNo;
  let agentId = body.agentId;
  let fixManId = body.fixManId;
  //get all slice
  //get all imei

  let ticketInfo = new ticketInfoService(agentId);
  (async ()=>{
    let update = await ticketInfo.updateFixTicketInfo(ticketNo, fixManId);
    res.send(new XCResult(true, {
      update:update
    }))
    return next();
  })().catch(err => {
    XCLogger.error('err occured:', err);
    res.send(new XCResult(false, new XCError(err, XC_BIZ_ERROR_TYPE.BIZ_ERROR)));
    return next();
  })

});

/**
 * 获取单条工单信息
 */
router.post('/single', function (req, res, next) {
  res.contentType = 'json';
  if (!existParam(req, 'ticketNo') || !existParam('agentId')) {
    let err = new XCError({}, XC_BIZ_ERROR_TYPE.INPUT_PARAMS_MISS);
    let result = new XCResult(false, err);
    res.send(result);
    return next();
  }

  let body = req.body;
  let ticketNo = body.ticketNo;
  let agentId = body.agentId;
  let ticketInfo = new ticketInfoService(agentId);
  
  (async ()=>{
    let singleTicket = await ticketInfo.getSingleTicketInfo(ticketNo, TICKET_TYPE.FIX);
    res.send(new XCResult(true, {
      ticketList:singleTicket
    }));
    return next();
  })().catch(err => {
    XCLogger.error('err occured:', err);
    res.send(new XCResult(false, new XCError(err, XC_BIZ_ERROR_TYPE.BIZ_ERROR)));
    return next();
  })

});

router.post('/fixed', function (req, res, next) {
  res.contentType = 'json';
  if (!existParam(req, 'ticketNo') || !existParam('agentId')) {
    let err = new XCError({}, XC_BIZ_ERROR_TYPE.INPUT_PARAMS_MISS);
    let result = new XCResult(false, err);
    res.send(result);
    return next();
  }

  let body = req.body;
  let ticketNo = body.ticketNo;
  let agentId = body.agentId;
  let ticketInfo = new ticketInfoService(agentId);
  
  (async ()=>{
    let singleTicket = await ticketInfo.closeTicket(ticketNo, TICKET_TYPE.FIX);
    res.send(new XCResult(true, {
      ticketList:singleTicket
    }));
    return next();
  })().catch(err => {
    XCLogger.error('err occured:', err);
    res.send(new XCResult(false, new XCError(err, XC_BIZ_ERROR_TYPE.BIZ_ERROR)));
    return next();
  })
})

module.exports = router;