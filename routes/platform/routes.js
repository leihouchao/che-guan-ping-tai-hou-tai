const Router = require('restify-router').Router;
const router = new Router();
//deviceInfo
router.add('/allDeviceInfo', require('./allDeviceInfo'));
router.add('/normalDeviceInfo', require('./normalDeviceInfo'));
router.add('/fixDeviceInfo', require('./fixDeviceInfo'));
router.add('/alarmDeviceInfo', require('./alarmDeviceInfo'));
router.add('/offLineDeviceInfo', require('./offLineDeviceInfo'));

//TicketInfo
router.add('/fixTicketInfo', require('./fixTicketInfo'));
router.add('/alarmTicketInfo', require('./alarmTicketInfo'));

//mainPage
router.add('/userSum',require('./userSum'));
router.add('/orderSum',require('./orderSum'));
router.add('/flowTransaction', require('./flowTransaction'));

//device Sum

router.add('/allDevSum', require('./allDeviceSum'));

router.add('/totalDevSum', require('./deviceSum'));
router.add('/brokenDevSum', require('./brokenDeviceSum'));
router.add('/alarmDevSum', require('./alarmDeviceSum'));
router.add('/normalDevSum', require('./normalDeviceSum'));
router.add('/offLineDevSum',require('./offLineDevSum'));
module.exports = router;