/**
 * Created by shun14 on 17-11-15.
 */
let Utils = require('../../utils/XCUtil');
let existQuery = Utils.existQuery;

let ErrorUtils = require('../../utils/XCErrorUtil');
let XCError = ErrorUtils.XCError;
let XC_BIZ_ERROR_TYPE = ErrorUtils.XC_BIZ_ERROR_TYPE;

let XCResult = require('../../utils/XCResultUtil').XCResult;
let XCLogger = require('../../utils/XCLogUtil').logger;
let platFormMainPageInfo = require('../../service/platFormMainPage').platFormMainPageInfo;

const Router = require('restify-router').Router;
const router = new Router();

router.get('', function (req, res, next) {
  res.contentType = 'json';
  if (!existQuery(req, 'agentId')) {
    let err = new XCError({}, XC_BIZ_ERROR_TYPE.INPUT_PARAMS_MISS);
    let result = new XCResult(false, err);
    res.send(result);
    return next();
  }

  //agentId
  
  let body = req.query;
  let agentId = body.agentId;

  let mainPage = new platFormMainPageInfo(agentId);
  (async ()=> {
      //TODO
    let arr = await mainPage.getTransactionSum();
    res.send(arr);
    return next();
  })().catch(err => {
    XCLogger.error('err occured:',err);
    res.send(new XCResult(false , new XCError(err, XC_BIZ_ERROR_TYPE.BIZ_ERROR)));
    return next();
  })
  
});


module.exports = router;