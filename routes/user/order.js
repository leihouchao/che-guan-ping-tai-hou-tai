/**
 * created on 20171107
 * @api {post} /eBike/user/order order
 * @apiName order
 * @apiVersion 1.0.0
 * @apiGroup user
 * @apiDescription 获取用户的行程-订单信息
 * @apiParam objectId 用户ID
 * @apiParamExample request:
 * {
 *     "objectId": "123456"
 * }
 * @apiSuccessExample response:
 * {
 *     "suc":true,
 *     "data":{}
 * }
 * 
 */

const Router = require('restify-router').Router;
const router = new Router();

router.post('', (req, res, next) => {
    if(!req.body){
        logger.info('no data!');
        res.send(new XCResult(false, new XCError("request no body!", ERR_TYPE.INPUT_PARAMS_MISS)));
        return next(); 
    }

    let objectId = req.body.objectId;
    /**
     * 查询用户历史行程-订单信息
     */

    res.send(new XCResult(true, ''));
    return next(); 
});

module.exports = router;