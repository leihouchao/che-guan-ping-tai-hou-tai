/**
 * created on 20171107
 * @api {post} /eBike/user/reservation reservation
 * @apiName reservation
 * @apiVersion 1.0.0
 * @apiGroup user
 * @apiDescription 预约用车
 * @apiParam objectId 用户Id 
 * @apiParam carId 车辆Id
 * @apiParamExample request:
 * {
 *     "objectId":"",
 *     "carId":""
 * }
 * @apiSuccessExample response:
 * {
 *     "suc":true,
 *     "data":"预约成功"
 * }
 */

const Router = require('restify-router').Router;
const router = new Router();
const logger = require('./../../utils/XCLogUtil');
const userOperationService = require('./../../service/userOperationService').userOperationService;

router.post('', (req, res, next) => {
    if(!req.body){
        logger.info('no data!');
        res.send(new XCResult(false, new XCError("request no body!", ERR_TYPE.INPUT_PARAMS_MISS)));
        return next(); 
    }
    let objectId = req.body.objectId;
    let carId = req.body.carId;
    let userOperition = new userOperationService(objectId, carId);
    userOperition.reserve()
    .then(result => {
        res.send(result);
        return next();
    })
    .catch(err => {
        res.send(new XCResult(false, new XCError(err, 0, '预约失败')));
        return next();
    })
});

module.exports = router;