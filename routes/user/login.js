/**
 * created on 20171107
 * @api {get} /user/login/:objectId login
 * @apiName login
 * @apiVersion 1.0.0
 * @apiGroup user
 * @apiDescription 用户登录
 * @apiParam objectId 用户ID
 * @apiParamExample request:
 * /user/login/123456
 * @apiSuccessExample reponse:
 * {
 *     "userInfo":{
 *         "name":"user",
 *         "tel": "155xxxxxxxx",
 *         "status": "ready"
 *     }
 * }
 * 
 */
const Router = require('restify-router').Router;
const router = new Router();

const logger = require('./../../utils/XCLogUtil');
const XCError = require('./../../utils/XCErrorUtil').XCError;
const XCResult = require('./../../utils/XCResultUtil').XCResult;
const ERR_TYPE = require('./../../utils/XCErrorUtil').XC_BIZ_ERROR_TYPE;

const XCEBike = require('./../../utils/XCEBikeUtils').XCEBike;
const XC_EBIKE_USER_STATE = XCEBike.XCEBikeUser.XC_EBIKE_USER_STATE;
const XC_EBIKE_DEVICE_STATE = XCEBike.XCEBikeDevice.XC_EBIKE_DEVICE_STATE;
const XCEBikeUser = XCEBike.XCEBikeUser;
const XCEBikeDevice = XCEBike.XCEBikeDevice;

router.get('/:objectId', (req, res, next) => {
    if(!req.params.hasOwnProperty('objectId')){
        logger.info('no objectId!');
        res.send(new XCResult(false, new XCError("request no objectId!", ERR_TYPE.INPUT_PARAMS_MISS)));
        return next(); 
    }

    let objectId = req.params.objectId;
    //获取用户的信息
    let user = new XCEBikeUser(objectId);
    // user.loadAllInfoSlow().then().catch();
    (async () => {
        await user.loadAllInfoSlow();
    })();
    res.send(new XCResult(true, user));
    return next(); 
});

module.exports = router;
