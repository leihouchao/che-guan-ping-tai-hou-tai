
/**
 * created on 20171113
 * @api {post} /eBike/user/pay pay
 * @apiName pay
 * @apiVersion 1.0.0
 * @apiGroup user
 * @apiDescription 支付行程订单
 * @apiParam objectId 用户ID
 * @apiParam chanel 支付方式
 * @apiParamExample request:
 * {
 *     "objectId": "123456",
 *     "chanel": "wx"
 * }
 * @apiSuccessExample response:
 * {
 *     "suc":true,
 *     "data":{
 *         // charge info
 *     }
 * }
 * 
 */

const Router = require('restify-router').Router;
const router = new Router();

const request = require('https');
const pingpp = require('./../../service/accountService').PingppService;
const createTradeNumber = require('./../../utils/helper').createTradeNumber;

const XCEBike = require('./../../utils/XCEBikeUtils').XCEBike;
const XCEBikeUserOrder = XCEBike.XCEBikeUserOrder;
const XCEBikeUser = XCEBike.XCEBikeUser;
const XC_EBIKE_USER_STATE = XCEBike.XCEBikeUser.XC_EBIKE_USER_STATE;

const logger = require('./../../utils/XCLogUtil').logger;
const XCError = require('./../../utils/XCErrorUtil').XCError;
const XCResult = require('./../../utils/XCResultUtil').XCResult;
const ERR_TYPE = require('./../../utils/XCErrorUtil').XC_BIZ_ERROR_TYPE;

router.post('', (req, res, next) => {
    if(!req.body || req.body.objectId === undefined || req.body.channel === undefined){
        res.send(new XCResult(false, new XCError("request no body or miss parameters!", ERR_TYPE.INPUT_PARAMS_MISS)));
        return next(); 
    }

    let user = new XCEBikeUser(req.body.objectId);
    user.setState(XC_EBIKE_USER_STATE.READY);
    res.send(new XCResult(true, 'charge success'));
    return next();  

  //ping++支付接口
    (async () => {
        let userOrderList = new XCEBikeUserOrder(req.body.objectId);
        let unpayOrder = userOrderList.getUnpaidCharge();
        if (unpayOrder.paidInfo === null || unpayOrder.paidInfo === undefined ||
            JSON.parse(unpayOrder.paidInfo).time_expire < parseInt(new Date().getTime() / 1000)) {
            let unpayOrderAmount = unpayOrder.cost;
            let chargeInfo = {
                subject: "电单车行程订单支付",
                body: "null",
                amount: unpayOrderAmount,
                channel: req.body.channel,
                client_ip: req.header('x-forwarded-for') || req.connection.remoteAddress,
                metadata: {objectId: req.body.objectId, service: "ebike-pay"},
                time_expire: parseInt(new Date(moment().utcOffset('+0800').add(15, 'days')).getTime() / 1000)
            };
            chargeInfo.order_no = createTradeNumber();
            let charge = await pingpp.createCharge(chargeInfo);
          
            if (charge.object !== 'charge') {
                throw charge;
            }
    
            let orderPaidInfo = JSON.stringify({
                order_id: charge.id,
                order_no: charge.order_no,
                channel: charge.channel,
                time_expire: charge.time_expire,
                time_paid: charge.time_paid
            }); 
            userOrderList.setOrderPaid(false, orderPaidInfo);
            res.send(new XCResult(true, charge));
            return next(); 
        } else {
            orderInfo = JSON.parse(unpayOrder.paidInfo);
            let charge = await pingpp.retrieveCharge(orderInfo.order_id);
            res.send(new XCResult(true, charge));
            return next(); 
        }
    })()
    .catch (error => {
        logger.error(error);
        res.send(new XCResult(false, new XCError(error, 0, '诚信金订单创建失败')));
        return next();
    });
});


module.exports = router;