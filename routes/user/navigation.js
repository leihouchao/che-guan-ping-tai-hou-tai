/**
 * created on 20171107
 * @api {post} /eBike/user/navigation/walking walking
 * @apiName walking
 * @apiVersion 1.0.0
 * @apiGroup user
 * @apiDescription 获取从当前位置到目的地的步行路线
 * @apiParam origin 起点坐标 
 * @apiParam destination 终点坐标
 * @apiParamExample request:
 * /eBike/user/navigation/walking?origin=lon,lat&destination=lon,lat
 * @apiSuccessExample response:
 * {
 *     "suc":true,
 *     "data":{
 *          "response": {
 *              "code": 0,
 *              "info": "",
 *              "polyline":[]
 *          }
 *     }
 * }
 * 
 */

const Router = require('restify-router').Router;
const router = new Router();

const XCResult = require('./../../utils/XCResultUtil').XCResult;
const walking = require('./../../service/navigationService').walkingNavigate;
const driving = require('./../../service/navigationService').drivingNavigate;

router.get('/walking', (req, res, next) => {
    if(!req.query){
        logger.info('no data!');
        res.send(new XCResult(false, new XCError("request no query!", ERR_TYPE.INPUT_PARAMS_MISS)));
        return next(); 
    }
    let origin = {
        lon: req.query.origin.split(',')[0],
        lat: req.query.origin.split(',')[1],
    }
    let destination = {
        lon: req.query.destination.split(',')[0],
        lat: req.query.destination.split(',')[1],
    }
     walking(origin, destination)
     .then(response => {
        res.send(new XCResult(true, response));
        return next(); 
     })
     .catch(err => {
        res.send(new XCResult(false, err));
        return next(); 
     })
    
});


/**
 * created on 20171107
 * @api {post} /eBike/user/navigation/driving driving
 * @apiName driving
 * @apiVersion 1.0.0
 * @apiGroup user
 * @apiDescription 获取从当前位置到目的地的行驶路线
 * @apiParam origin 起点坐标 
 * @apiParam destination 终点坐标
 * @apiParamExample request:
 * /eBike/user/navigation/driving?origin=lon,lat&destination=lon,lat
 * @apiSuccessExample response:
 * {
 *     "suc":true,
 *     "data":{
 *          "response": {
 *              "code": 0,
 *              "info": "",
 *              "polyline":[]
 *          }
 *     }
 * }
 * 
 */
router.get('/driving', (req, res, next) => {
    if(!req.query){
        logger.info('no data!');
        res.send(new XCResult(false, new XCError("request no query!", ERR_TYPE.INPUT_PARAMS_MISS)));
        return next(); 
    }
    let origin = {
        lon: req.query.origin.split(',')[0],
        lat: req.query.origin.split(',')[1],
    }
    let destination = {
        lon: req.query.destination.split(',')[0],
        lat: req.query.destination.split(',')[1],
    }
    driving(origin, destination)
    .then(response => {
        res.send(new XCResult(true, response));
        return next(); 
    })
    .catch(err => {
        res.send(new XCResult(false, err));
        return next(); 
    })
});


module.exports = router;