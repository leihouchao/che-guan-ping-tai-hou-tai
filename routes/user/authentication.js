/**
 * created on 20171107
 * @api {psot} /eBike/user/auth auth
 * @apiName auth
 * @apiVersion 1.0.0
 * @apiGroup user
 * @apiDescription 用户身份认证
 * @apiParam userInfo 用户信息
 * @apiParamExample request:
 * {
 *     "userInfo":{
 *         "objectId":"",
 *         "name":"xxx",
 *         "cert_no":"xxxxxxxx"
 *     }
 * }
 * @apiSuccessExample response:
 * {
 *     "suc":true,
 *     "data":{}
 * }
 * 
 */

const Router = require('restify-router').Router;
const router = new Router();

const logger = require('./../../utils/XCLogUtil');
const XCError = require('./../../utils/XCErrorUtil').XCError;
const XCResult = require('./../../utils/XCResultUtil').XCResult;
const ERR_TYPE = require('./../../utils/XCErrorUtil').XC_BIZ_ERROR_TYPE;
const User = require('./../../service/userService').User;

router.post('', (req, res, next) => {
    if(!req.body){
        logger.info('no data!');
        res.send(new XCResult(false, new XCError("request no body!", ERR_TYPE.INPUT_PARAMS_MISS)));
        return next(); 
    }
    let userInfo = req.body.userInfo;
    let user = new User(userInfo.objectId);
    user.authenticate(userInfo)
    .then(result => {
        if(result.success){
            res.send(new XCResult(true, result));
        }else{
            res.send(new XCResult(false, result));
        }
        return next();
    })
    .catch(error => {
        logger.error('authentication.js error: ', error);
        res.send(new XCResult(false, error));
        return next();
    })
});



module.exports = router;