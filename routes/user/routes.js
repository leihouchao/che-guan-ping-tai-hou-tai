/**
 * 20171107
 */

const Router = require('restify-router').Router;
const router = new Router();

router.add('/register', require('./register'));
router.add('/auth', require('./authentication'));
router.add('/deposit', require('./deposit'));
router.add('/login', require('./login'));
router.add('/reservation', require('./reservation'));
router.add('/cancellation', require('./cancellation'));
router.add('/order', require('./order'));
router.add('/navigation', require('./navigation'));
router.add('/repair', require('./repair'));
router.add('/pay', require('./pay'));
router.add('pingpp-webhooks', require('./webhooks'));  // 仅供ping++支付回调使用

module.exports = router;