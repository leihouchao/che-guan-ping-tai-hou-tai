/**
 * created on 20171107
 * @api {post} /eBike/user/register register
 * @apiName register
 * @apiVersion 1.0.0
 * @apiGroup user
 * @apiDescription 保存用户的注册信息和注册记录
 * @apiParam tel 电话号码
 * @apiParam objectId 用户Id
 * @apiParamExample request:
 *  {   
 *      "tel": "155xxxxxxxx",
 *      "objectId": "********"
 *  }
 * @apiSuccessExample response:
 * {
 *     "suc":true,
 *     "data":""
 * }
 * 
 */

const Router = require('restify-router').Router;
const router = new Router();

const logger = require('./../../utils/XCLogUtil');
const XCError = require('./../../utils/XCErrorUtil').XCError;
const XCResult = require('./../../utils/XCResultUtil').XCResult;
const ERR_TYPE = require('./../../utils/XCErrorUtil').XC_BIZ_ERROR_TYPE;
const User = require('./../../service/userService').User;

router.post('', (req, res, next) =>{
    if(!req.body){
        logger.info('no data!');
        res.send(new XCResult(false, new XCError("request no body!", ERR_TYPE.INPUT_PARAMS_MISS)));
        return next(); 
    }
    let objectId = req.body.objectId;
    let tel = req.body.tel;
    let user = new User(objectId);
    user.register(objectId, tel)
    .then(result => {
        res.send(result);
        return next();
    })
    .catch(err => {
        res.send(new XCResult(false, new XCError(err, 0, '注册失败')));
        return next();
    })
});

module.exports = router;