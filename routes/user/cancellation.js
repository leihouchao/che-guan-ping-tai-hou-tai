/**
 * created on 20171107
 * @api {post} /eBike/user/cancellation cancellation
 * @apiName cancellation
 * @apiVersion 1.0.0
 * @apiGroup user
 * @apiDescription 取消预约
 * @apiParam objectId 用户Id 
 * @apiParam carId 车辆Id
 * @apiParamExample request:
 * {
 *     "objectId":"",
 *     "carId":""
 * }
 * @apiSuccessExample response:
 * {
 *     "suc":true,
 *     "data":"取消预约成功"
 * }
 * 
 */
 
const Router = require('restify-router').Router;
const router = new Router();

const userOperationService = require('./../../service/userOperationService').userOperationService;

router.post('', (req, res, next) => {
    if(!req.body){
        logger.info('no data!');
        res.send(new XCResult(false, new XCError("request no body!", ERR_TYPE.INPUT_PARAMS_MISS)));
        return next(); 
    }

    let objectId = req.body.objectId;
    let carId = req.body.carId;
    let userOperation = new userOperationService(objectId, carId);
    userOperation.cancel()
    .then(result => {
        res.send(result);
        return next();
    })
    .catch(err => {
        res.send(new XCResult(false, err));
        return next();
    })
});

module.exports = router;