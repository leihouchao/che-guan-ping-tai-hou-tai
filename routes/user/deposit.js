/**
 * created on 20171107
 * @api {post} /eBike/user/deposit deposit
 * @apiName deposit
 * @apiVersion 1.0.0
 * @apiGroup user
 * @apiDescription 支付押金
 * @apiParam objectId 用户ID
 * @apiParam chanel 支付方式
 * @apiParamExample request:
 * {
 *     "objectId": "123456",
 *     "chanel": "wx"
 * }
 * @apiSuccessExample response:
 * {
 *     "suc":true,
 *     "data":{
 *         // charge info
 *     }
 * }
 * 
 */

const Router = require('restify-router').Router;
const router = new Router();

const pingpp = require('./../../service/accountService').PingppService;
const deposit = require('./../../biz/charges.json').deposit;
const createTradeNumber = require('./../../utils/helper').createTradeNumber;

const XCEBike = require('./../../utils/XCEBikeUtils').XCEBike;
const XCEBikeUser = XCEBike.XCEBikeUser;
const XC_EBIKE_USER_STATE = XCEBike.XCEBikeUser.XC_EBIKE_USER_STATE;

// const XC_EBIKE_USER_STATE = xcdsutils.XCEBikeUser.XC_EBIKE_USER_STATE;

const logger = require('./../../utils/XCLogUtil').logger;
const XCError = require('./../../utils/XCErrorUtil').XCError;
const XCResult = require('./../../utils/XCResultUtil').XCResult;
const ERR_TYPE = require('./../../utils/XCErrorUtil').XC_BIZ_ERROR_TYPE;

const moment = require('moment');

router.post('', (req, res, next) => {
    if(!req.body || req.body.objectId === undefined || req.body.channel === undefined){
        res.send(new XCResult(false, new XCError("request no body or miss parameters!", ERR_TYPE.INPUT_PARAMS_MISS)));
        return next();
    }

    let user = new XCEBikeUser(req.body.objectId);
    user.setState(XC_EBIKE_USER_STATE.AUTHED);
    res.send(new XCResult(true, 'charge success'));
    return next(); 
    //ping++支付接口
    (async () => {
        let user = new XCEBikeUser(req.body.objectId);
        let userInfo = {};// user._readDBInfo();
        if (userInfo.depositedInfo === null || userInfo.depositedInfo === undefined ||
            JSON.parse(userInfo.depositedInfo).time_expire < parseInt(new Date().getTime() / 1000)) {
            let chargeInfo = deposit;
            chargeInfo.channel = req.body.channel;
            chargeInfo.client_ip = req.header('x-forwarded-for') || req.connection.remoteAddress;
            chargeInfo.metadata = {objectId: req.body.objectId, service: "ebike-deposit"};
            chargeInfo.order_no = createTradeNumber();
            chargeInfo.time_expire = parseInt(new Date(moment().utcOffset('+0800').add(1, 'hours')).getTime() / 1000);
            
            let charge = await pingpp.createCharge(chargeInfo);
            
            if (charge.object !== 'charge') {
                throw charge;
            }
            let depositedInfo = JSON.stringify({
                order_id: charge.id,
                order_no: charge.order_no,
                channel: charge.channel,
                time_paid: charge.time_paid, 
                time_expire: charge.time_expire
            });
            user.deposited(depositedInfo);
            res.send(new XCResult(true, charge));
            return next();
        } else {
            orderInfo = JSON.parse(userInfo.depositedInfo);
            let charge = await pingpp.retrieveCharge(orderInfo.order_id);
            res.send(new XCResult(true, charge));
            return next(); 
        }
    })()
    .catch (error => {
        logger.error(error);
        res.send(new XCResult(false, new XCError(error, 0, '诚信金订单创建失败')));
        return next();
    });
});


module.exports = router;

