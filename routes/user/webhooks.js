/**
 * 20171113
 */

const Router = require('restify-router').Router;
const router = new Router();

const xcdsutils = require('xcdsutils');
const XCEBikeUser = xcdsutils.XCEBikeUser.XCEBikeUser;
const XC_EBIKE_USER_STATE = xcdsutils.XCEBikeUser.XC_EBIKE_USER_STATE;
const XCEBikeUserOrder = xcdsutils.XCEBikeUserOrder.XCEBikeUserOrder;

const logger = require('./../../utils/XCLogUtil');
const XCError = require('./../../utils/XCErrorUtil').XCError;
const XCResult = require('./../../utils/XCResultUtil').XCResult;
const ERR_TYPE = require('./../../utils/XCErrorUtil').XC_BIZ_ERROR_TYPE;

router.post('', (req, res, next) => {
    (async () => {
        let event = req.body;
        if (event.type === undefined) {
            res.send({
                code: 500,
                error: "Event 对象中缺少 type 字段"
            });
            return next();
        }

        switch (event.type) {
            case "charge.succeeded":
                let charge = event.data.object;
                switch (charge.metadata.service) {
                    case "ebike-deposit":{
                        let user = new XCEBikeUser(charge.metadata.objectId);
                        let depositedInfo = JSON.stringify({
                            order_id: charge.id,
                            order_no: charge.order_no,
                            channel: charge.channel,
                            time_paid: charge.time_paid
                        });
                        user.setDepositStatus(XC_EBIKE_USER_STATE.READY, depositedInfo);
        
                        res.send({
                            code: 200,
                            data: 'OK'
                        });
                        break;
                    }
                    case "ebike-pay":
                        let user = new XCEBikeUser(charge.metadata.objectId);
                        let userOrderList = new XCEBikeUserOrder(charge.metadata.objectId);
                        let orderPaidInfo = JSON.stringify({
                            order_id: charge.id,
                            order_no: charge.order_no,
                            channel: charge.channel,
                            time_paid: charge.time_paid
                        });
                        user.setState(XC_EBIKE_USER_STATE.READY);
                        userOrderList.setOrderPaid(true, orderPaidInfo);
                        
                        res.send({
                            code: 200,
                            data: 'OK'
                        });
                        break;
                    default:
                        res.send({
                            code: 500,
                            error: "非对应服务器"
                        });
                        break;
                }
                break;
            case "charge.succeeded":
                // 对退款异步通知的处理代码
                res.send({
                    code: 200,
                    data: 'OK'
                });
                break;
            default:
                res.send({
                    code: 500,
                    error: "未知 Event 类型"
                });
                break;
        }
        return next();
    })()
    .catch (error => {
        logger.error(error);
        res.send({
            code: 500,
            error: '服务器接口错误'
        });
        return next();
    });
});

module.exports = router;