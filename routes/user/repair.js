/**
 * created on 20171107
 * @api {get} /eBike/user/repair repair
 * @apiName repair
 * @apiVersion 1.0.0
 * @apiGroup user
 * @apiDescription 用户报修
 * @apiParam objectId 用户Id 
 * @apiParam carId 车辆Id
 * @apiParamExample request:
 * {
 *      "objectId":"",
 *      "carId":""
 * }
 * @apiSuccessExample response:
 * {
 *     "suc":true,
 *     "data":"报修成功"
 * }
 * 
 */

const Router = require('restify-router').Router;
const router = new Router();

router.post('', (req, res, next) => {
    if(!req.body){
        logger.info('no data!');
        res.send(new XCResult(false, new XCError("request no body!", ERR_TYPE.INPUT_PARAMS_MISS)));
        return next(); 
    }

    let objectId = req.body.objectId;
    let carId = req.body.carId;
    /**
     * 将报修的信息存到数据库中，车辆状态 => 故障，用户状态 => READY，发送锁车命令；
     */

    res.send(new XCResult(true, '报修成功'));
    return next(); 
});

module.exports = router;